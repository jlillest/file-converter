import sys
import contextlib
import tkinter
from tkinter import filedialog

from mnemo_file import MNemoFile

VERSION="0.1.3"


@contextlib.contextmanager
def text_box_print(text_box):
    text_box.configure(state="normal")
    yield
    text_box.see("end")
    text_box.configure(state="disable")


class MNemoConverter(tkinter.Tk):
    def __init__(self):
        tkinter.Tk.__init__(self)
        self.title("MNemo File Converter v{}".format(VERSION))

        self.main_menu = tkinter.Menu(self, tearoff=0)
        self.main_menu.add_command(label="Convert MNemo File",
                                   command=self.convert_file)
        self.main_menu.add_command(label="Quit", command=self.destroy)
        self.config(menu=self.main_menu)

        text_box_args = {"height": 30,
                         "width":  80,
                         "state":  "disabled",
                         "background": "black",
                         "foreground": "white",
                        }
        self.text_box = tkinter.Text(self, **text_box_args)
        self.text_box.pack(side=tkinter.LEFT)

        self.scroll_bar = tkinter.Scrollbar(self)
        self.scroll_bar.pack(side=tkinter.RIGHT, fill="y")

        self.mainloop()

    def print(self, message):
        with text_box_print(self.text_box):
            self.text_box.insert(tkinter.END, message + "\n")

    def convert_file(self):
        self.print("choose file to convert...")

        file_options = { "filetypes" : [
                                        ('excel files', '.xlsx'),
                                       ],
                         "parent" : self,
                       }

        mnemo_filename = filedialog.askopenfilename(**file_options)
        if not mnemo_filename:
            self.print("cancelled")
            return

        self.print("converting {}".format(mnemo_filename))
        mnemo_file = MNemoFile(mnemo_filename, self.print)
        
        file_options = { "filetypes" : [('walls files', '.SRV')],
                         "parent" : self,
                       }

        walls_filename = filedialog.asksaveasfilename(**file_options)
        if not walls_filename:
            self.print("cancelled")
            return

        self.print("saving to walls file: {}".format(walls_filename))
        mnemo_file.convert_to_walls(walls_filename)

def run_app():
    try:
        window = MNemoConverter()
    except tkinter._tkinter.TclError:
        print("No Tk installed")
        # go to command line options

if __name__ == '__main__':
    run_app()
    sys.exit(0)
