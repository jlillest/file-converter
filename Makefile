VIRTUALENV?=virtualenv
PYTHON3=$(shell which python3)

ALL: mnemo-converter

mnemo-converter:
	$(VIRTUALENV)/bin/pyinstaller --onefile --name mnemo-converter mnemo_converter.py

$(VIRTUALENV)/bin/activate:
	test -d $(VIRTUALENV) || virtualenv -p $(PYTHON3) $(VIRTUALENV)
	$(VIRTUALENV)/bin/pip install -r requirements.txt

setup_virtualenv: $(VIRTUALENV)/bin/activate
	$(echo run the following to enter the virtualenv: \"source $(VIRTUALENV)/bin activate\")

run_unit_tests: setup_virtualenv
	$(VIRTUALENV)/bin/python test.py

clean:
	$(RM) -rf build/
	$(RM) -rf dist/
	$(RM) *.pyc
