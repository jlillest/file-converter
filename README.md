MNemo file converter
====================
convert MNemo survey files to Walls format

[![build status](https://gitlab.com/jlillest/file-converter/badges/master/pipeline.svg)](https://gitlab.com/jlillest/file-converter/commits/master)


Latest Linux build here:
[Linux Build](https://gitlab.com/jlillest/file-converter/-/jobs/artifacts/master/raw/dist/mnemo-converter?job=build:linux)

Latest Windows build here:
[Windows Build](https://ci.appveyor.com/project/JonLill/file-converter)