import os
import xlrd
from enum import Enum

class SurveyFileState(Enum):
    START = 0
    UNITS = 1
    DATE = 2
    HEADERS = 3
    DATA = 4
    DONE = 5

class SurveyShot:
    def __init__(self, **kwargs):
        default_keys = ["prefix",
                        "from_station",
                        "to_station",
                        "distance",
                        "azimuth",
                        "from_depth",
                        "to_depth",
                       ]

        for key in default_keys:
            setattr(self, key, kwargs[key])
            
        for lrud in ["left", "right", "up", "down"]:
            value = kwargs[lrud] if lrud in kwargs else 0
            setattr(self, lrud, value)

    def __str__(self):
        return "SurveyShot: {0}{1} to {0}{2}".format(self.prefix,
                                                     self.from_station,
                                                     self.to_station)

class SurveyChunk(list):
    def __init__(self, **kwargs):
        self.state = SurveyFileState.START
        self.units = None
        self.date = None
        self.forward = True
        self.imperial = True
        self.current_station = 0
        self.shots = []

        self.prefix = kwargs["prefix"] if "prefix" in kwargs else "A"

        if "sheet" in kwargs:
            self.get_sheet(kwargs["sheet"])

        self.state = SurveyFileState.DONE

    def get_sheet(self, sheet):
        for i in range(sheet.nrows):
            row = sheet.row_values(i)
            if self.current_state(SurveyFileState.START):
                if row[0] == "BAS":
                    self.set_state(SurveyFileState.UNITS)
            elif self.current_state(SurveyFileState.UNITS):
                if row[0].startswith("Direction"):
                    self.get_units(row)
                    self.set_state(SurveyFileState.DATE)
            elif self.current_state(SurveyFileState.DATE):
                if row[0].startswith("Date"):
                    self.get_date(row)
                    self.set_state(SurveyFileState.HEADERS)
            elif self.current_state(SurveyFileState.HEADERS):
                # eventually may need to validate position of data
                self.set_state(SurveyFileState.DATA)

            elif self.current_state(SurveyFileState.DATA):
                if not row[0] == "STD":
                    continue
                self.current_station += 1
                self.shots.append(SurveyShot(prefix=self.prefix,
                                             from_station=self.current_station,
                                             to_station=self.current_station + 1,
                                             distance=row[1],
                                             azimuth=row[2],
                                             from_depth=row[6],
                                             to_depth=row[7],
                                            )
                                 )

    def current_state(self, state):
        return state == self.state

    def set_state(self, state):
        self.state = state

    def get_units(self, row):
        self.forward = "SURVEY_IN" in row[0]
        self.units = "Imperial" in row[1]

    def get_date(self, row):
        #print("Date is {}".format(row[0].replace("Date: ", "")))
        pass


class MNemoFile:
    def __init__(self, filename, print_function):
        self.chunks = []
        self.prefix = "A"
        self.print = print_function
        mnemo_workbook = xlrd.open_workbook(filename)
        self.get_chunks(mnemo_workbook)

    def get_chunks(self, workbook):
        for sheet in workbook.sheets():
            self.print("processing sheet: %s with prefix: %s" % (sheet.name, self.prefix))
            self.chunks.append(SurveyChunk(sheet=sheet, prefix=self.prefix))
            self.prefix = chr(ord(self.prefix) + 1)
            
    def convert_to_walls(self, filename):
        with open(filename, 'w') as f:
            self.write(f, ";Converted from MNemo data")
            self.write(f)
            self.write(f)
            for chunk in [_ for _ in self.chunks if _.shots]:
                self.write_chunk(f, chunk)

    def write_chunk(self, f, chunk):
        self.write(f, "#PREFIX {}".format(chunk.prefix))
        self.write(f, "#UNITS ORDER=DA TAPE=SS")
        for shot in chunk.shots:
            data = [shot.from_station,
                    shot.to_station,
                    shot.distance,
                    shot.azimuth,
                    shot.from_depth,
                    shot.to_depth,
                   ]
            self.write(f, "\t".join([str(_) for _ in data]))
        self.write(f)
        self.write(f)
            
    def write(self, f, text=""):
        f.write(text + "\n")
